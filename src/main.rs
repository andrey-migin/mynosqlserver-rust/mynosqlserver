use std::sync::Arc;

use app::AppServices;

mod app;
mod db;
mod http;
mod json;
mod utils;

#[async_std::main]
async fn main() {
    let app = AppServices::new();
    let app = Arc::new(app);

    http::http_server::start(app).await;
}
