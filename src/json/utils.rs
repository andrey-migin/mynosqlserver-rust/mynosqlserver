use std::collections::HashMap;

use super::JsonFirstLine;

pub fn compile_json_bytes(json_data: &HashMap<&str, JsonFirstLine>) -> Vec<u8> {
    let mut result = Vec::new();

    result.push(super::consts::OPEN_BRACKET);
    for (_, itm) in json_data {
        result.extend(itm.name);
        result.push(super::consts::DOUBLE_COLUMN);
        result.extend(itm.value);
    }
    result.push(super::consts::CLOSE_BRACKET);
    result
}
