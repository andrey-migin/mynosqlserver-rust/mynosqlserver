use std::collections::HashMap;

use crate::json::JsonFirstLine;

fn get_str_value<'t>(
    data: &HashMap<&str, JsonFirstLine<'t>>,
    field: &str,
) -> Result<&'t str, String> {
    let result = data.get(field);
    match result {
        Some(line) => Ok(std::str::from_utf8(line.value).unwrap()), //TODO - убрать unwrap - сделать типизацию
        None => Err(format!("Can not find field {}", field)),
    }
}
