use crate::{
    db::{DbInstance, FailOperationResult, OperationResult},
    json::db_entity::DbEntity,
};

use super::types::PersistencePeriod;

impl DbInstance {
    pub async fn insert(
        &self,
        table_name: &str,
        payload: &[u8],
    ) -> Result<OperationResult, FailOperationResult> {
        let mut write_access = self.tables.write().await;

        let table = write_access.get_mut(table_name).ok_or_else(|| {
            FailOperationResult::TableAlreadyExist {
                table_name: table_name.to_string(),
            }
        })?;

        let db_entity = DbEntity::parse(payload)?;

        table.insert(db_entity)?;

        return Ok(OperationResult::Ok);
    }

    pub async fn insert_or_replace(
        &self,
        table_name: &str,
        payload: &[u8],
    ) -> Result<OperationResult, FailOperationResult> {
        let mut write_access = self.tables.write().await;

        let table = write_access.get_mut(table_name).ok_or_else(|| {
            FailOperationResult::TableAlreadyExist {
                table_name: table_name.to_string(),
            }
        })?;

        let db_entity = DbEntity::parse(payload)?;

        table.insert_or_replace(db_entity)?;

        return Ok(OperationResult::Ok);
    }
}
