use crate::json::db_entity::DbEntity;

pub struct DbRow {
    pub row_key: String,
    pub data: Vec<u8>,
    pub expires: Option<i64>,
    pub time_stamp: i64,
}

impl DbRow {
    pub fn new<'s>(src: &DbEntity<'s>, time_stamp: i64) -> Self {
        return Self {
            row_key: src.row_key.to_string(),
            data: src.raw.to_vec(),
            expires: src.expires,
            time_stamp,
        };
    }
}
