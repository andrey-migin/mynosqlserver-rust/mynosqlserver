use std::{collections::BTreeMap, usize};

use crate::json::{self, db_entity::DbEntity};

use super::{date_time, DbPartition, DbRow, FailOperationResult, OperationResult};

pub struct DbTable {
    pub id: String,
    pub persist: bool,
    pub created: i64,
    partitions: BTreeMap<String, DbPartition>,
}

impl DbTable {
    pub fn new(id: &str, persist: bool, created: i64) -> Self {
        DbTable {
            id: id.to_string(),
            persist,
            created,
            partitions: BTreeMap::new(),
        }
    }

    pub async fn bulk_insert_or_replace(
        &self,
        records: Vec<&[u8]>,
    ) -> Result<(), FailOperationResult> {
        for bytes in records {
            let entities_to_insert_or_replace = json::parser::parse_first_line(bytes)?;

            for el in entities_to_insert_or_replace {
                todo!("Implement")
            }
        }

        Ok(())
    }

    pub fn insert<'s>(&mut self, db_entity: DbEntity<'s>) -> Result<(), FailOperationResult> {
        if !self.partitions.contains_key(&db_entity.partition_key) {
            self.partitions
                .insert(db_entity.partition_key.to_string(), DbPartition::new());
        }

        let partition = self.partitions.get_mut(&db_entity.partition_key).unwrap();

        if partition.rows.contains_key(&db_entity.row_key) {
            return Err(FailOperationResult::RecordAlreadyExists);
        }

        let now_time = date_time::get_utc_now();

        partition.rows.insert(
            db_entity.row_key.to_string(),
            DbRow::new(&db_entity, now_time),
        );

        Ok(())
    }

    pub fn insert_or_replace<'s>(
        &mut self,
        db_entity: DbEntity<'s>,
    ) -> Result<(), FailOperationResult> {
        if !self.partitions.contains_key(&db_entity.partition_key) {
            self.partitions
                .insert(db_entity.partition_key.to_string(), DbPartition::new());
        }

        let partition = self.partitions.get_mut(&db_entity.partition_key).unwrap();

        partition.rows.remove(&db_entity.row_key);

        partition.rows.insert(
            db_entity.row_key.to_string(),
            DbRow::new(&db_entity, date_time::get_utc_now()),
        );

        Ok(())
    }

    pub fn get_all(&self) -> Result<OperationResult, FailOperationResult> {
        let mut json = Vec::new();

        for db_partition in self.partitions.values() {
            for db_row in db_partition.rows.values() {
                if json.len() == 0 {
                    json.push(crate::json::consts::OPEN_ARRAY);
                } else {
                    json.push(crate::json::consts::COMMA);
                }

                json.extend(db_row.data.as_slice());
            }
        }

        json.push(crate::json::consts::CLOSE_ARRAY);

        Ok(OperationResult::OkWithJson { json })
    }

    pub fn get_partition(&self, partition_key: &str) -> OperationResult {
        let mut json = Vec::new();

        let db_partition = self.partitions.get(partition_key);

        if db_partition.is_none() {
            json.push(crate::json::consts::OPEN_ARRAY);
            json.push(crate::json::consts::CLOSE_ARRAY);
            return OperationResult::OkWithJson { json };
        }

        let db_partition = db_partition.unwrap();

        for db_row in db_partition.rows.values() {
            if json.len() == 0 {
                json.push(crate::json::consts::OPEN_ARRAY);
            } else {
                json.push(crate::json::consts::COMMA);
            }

            json.extend(db_row.data.as_slice());
        }

        json.push(crate::json::consts::CLOSE_ARRAY);

        return OperationResult::OkWithJson { json };
    }

    pub fn get_by_row_key(&self, row_key: &str) -> OperationResult {
        let mut json = Vec::new();

        json.push(crate::json::consts::OPEN_ARRAY);

        let no: usize = 0;

        for db_partition in self.partitions.values() {
            for db_row in db_partition.rows.values() {
                json.extend(db_row.data.as_slice());
            }
        }

        json.push(crate::json::consts::CLOSE_ARRAY);

        return OperationResult::OkWithJson { json };
    }

    pub fn get_row(
        &self,
        partition_key: &str,
        row_key: &str,
    ) -> Result<OperationResult, FailOperationResult> {
        let db_partition = self.partitions.get(partition_key);

        if db_partition.is_none() {
            return Err(FailOperationResult::RecordNotFound);
        }

        let db_partition = db_partition.unwrap();

        let db_row = db_partition.rows.get(row_key);

        if db_row.is_none() {
            return Err(FailOperationResult::RecordNotFound);
        }

        let db_row = db_row.unwrap();

        return Ok(OperationResult::OkWithJson {
            json: db_row.data.clone(),
        });
    }
}
