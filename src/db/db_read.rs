use std::collections::HashMap;

use async_std::sync::RwLockReadGuard;

use crate::db::{DbInstance, DbTable, FailOperationResult, OperationResult};

impl DbInstance {
    pub async fn get_rows(
        &self,
        table_name: &str,
        partition_key: Option<String>,
        row_key: Option<String>,
    ) -> Result<OperationResult, FailOperationResult> {
        let read_access = self.tables.read().await;

        let table = get_table(&read_access, table_name)?;

        if partition_key.is_none() && row_key.is_none() {
            return table.get_all();
        }

        if partition_key.is_some() && row_key.is_none() {
            let result = table.get_partition(partition_key.unwrap().as_str());
            return Ok(result);
        }

        if partition_key.is_none() && row_key.is_some() {
            let result = table.get_by_row_key(row_key.unwrap().as_str());
            return Ok(result);
        }

        let partition_key = partition_key.unwrap();
        let row_key = row_key.unwrap();

        return table.get_row(partition_key.as_str(), row_key.as_str());
    }
}

fn get_table<'s>(
    read_access: &'s RwLockReadGuard<'s, HashMap<String, DbTable>>,
    table_name: &'s str,
) -> Result<&'s DbTable, FailOperationResult> {
    let result = read_access.get(table_name);

    return match result {
        Some(db_table) => Ok(db_table),
        None => Err(FailOperationResult::TableNotFound {
            table_name: table_name.to_string(),
        }),
    };
}
