use super::{
    types::{FailOperationResult, OperationResult},
    DbTable,
};
use crate::utils::*;
use async_std::sync::{RwLock, RwLockReadGuard};
use std::collections::HashMap;

pub struct DbInstance {
    pub tables: RwLock<HashMap<String, DbTable>>,
}

pub struct TableAccess<'s> {
    read_access: RwLockReadGuard<'s, HashMap<String, DbTable>>,
    name: &'s str,
}

impl<'s> TableAccess<'s> {
    pub fn table(&'s self) -> &'s DbTable {
        return self.read_access.get(self.name).unwrap();
    }
}

impl DbInstance {
    pub fn new() -> DbInstance {
        DbInstance {
            tables: RwLock::new(HashMap::new()),
        }
    }

    pub async fn create_table(
        &self,
        name: &str,
        persist: bool,
    ) -> Result<OperationResult, FailOperationResult> {
        let mut tables_write_access = self.tables.write().await;

        if tables_write_access.contains_key(name) {
            return Err(FailOperationResult::TableAlreadyExist {
                table_name: name.to_string(),
            });
        }

        let new_table = DbTable::new(name, persist, now_as_unix_time());
        tables_write_access.insert(name.to_string(), new_table);

        return Ok(OperationResult::Ok);
    }

    pub async fn remove_table(&self, name: &str) -> Result<OperationResult, FailOperationResult> {
        let mut tables_write_access = self.tables.write().await;

        if !tables_write_access.contains_key(name) {
            return Err(FailOperationResult::TableNotFound {
                table_name: name.to_string(),
            });
        }

        tables_write_access.remove(name);

        return Ok(OperationResult::Ok);
    }

    pub async fn get_table_names(&self) -> Vec<String> {
        let mut result = Vec::new();

        let read_access = self.tables.read().await;
        for table_name in read_access.keys() {
            result.push(table_name.to_string());
        }

        return result;
    }

    pub async fn get_table<'s>(
        &'s self,
        table_name: &'s str,
    ) -> Result<TableAccess<'s>, FailOperationResult> {
        let read_access = self.tables.read().await;

        let result = read_access.get(table_name);

        return match result {
            Some(_) => {
                let result: TableAccess = TableAccess {
                    read_access: read_access,
                    name: table_name,
                };
                Ok(result)
            }
            None => Err(FailOperationResult::TableNotFound {
                table_name: table_name.to_string(),
            }),
        };
    }
}
