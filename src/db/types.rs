pub enum OperationResult {
    Ok,
    OkWithJson { json: Vec<u8> },
}

#[derive(Debug)]
pub enum FailOperationResult {
    TableAlreadyExist { table_name: String },
    TableNotFound { table_name: String },
    InvalidJson { err: String },
    FieldPartitionKeyIsRequired,
    FieldRowKeyIsRequired,
    RowKeyNotFound,
    RecordAlreadyExists,
    RecordNotFound,
}

pub enum PersistencePeriod {
    Immediately,
    Sec1,
    Sec5,
    Sec15,
    Sec30,
    Min1,
    Asap,
}
