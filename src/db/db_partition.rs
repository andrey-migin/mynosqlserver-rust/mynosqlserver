use std::collections::BTreeMap;

use super::DbRow;

pub struct DbPartition {
    pub rows: BTreeMap<String, DbRow>,
}

impl DbPartition {
    pub fn new() -> DbPartition {
        DbPartition {
            rows: BTreeMap::new(),
        }
    }
}
