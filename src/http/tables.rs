use crate::{app::AppServices, db::OperationResult};
use std::sync::Arc;
use tide::{prelude::*, Body, Response, StatusCode};

use super::http_helpers;
#[derive(Deserialize, Serialize)]
pub struct TableJsonResult<'t> {
    pub name: &'t str,
}

pub async fn list_of_tables(req: tide::Request<Arc<AppServices>>) -> tide::Result {
    let state = req.state();

    let result = state.db.get_table_names().await;

    let mut res = Response::new(StatusCode::Ok);

    res.set_body(Body::from_json(&result)?);

    Ok(res)
}

#[derive(Deserialize, Serialize)]
pub struct CreateTableRequestModel {
    #[serde(rename(deserialize = "tableName"))]
    pub table_name: String,

    pub persist: Option<bool>,
}

pub async fn create_table(req: tide::Request<Arc<AppServices>>) -> tide::Result {
    let model: CreateTableRequestModel = req.query()?;

    let state = req.state();

    let result = state.db.create_table(&model.table_name, false).await;

    return http_helpers::get_http_response(result);
}

pub async fn create_table_if_not_exists(req: tide::Request<Arc<AppServices>>) -> tide::Result {
    let model: CreateTableRequestModel = req.query()?;

    let state = req.state();

    let result = state.db.create_table(&model.table_name, false).await;

    return http_helpers::get_http_response(result);
}

#[derive(Deserialize, Serialize)]
pub struct RemoveTableRequestModel {
    pub table_name: String,
}

pub async fn remove_table(req: tide::Request<Arc<AppServices>>) -> tide::Result {
    let request_model: RemoveTableRequestModel = req.query()?;
    let state = req.state();

    let result = state.db.remove_table(&request_model.table_name).await;
    return http_helpers::get_http_response(result);
}
