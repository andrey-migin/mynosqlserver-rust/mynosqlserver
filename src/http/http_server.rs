use async_std::fs;
use tide::Redirect;

use crate::app::AppServices;
use std::sync::Arc;

use super::{api, bulk, row, tables};

pub async fn start(app: Arc<AppServices>) {
    let mut app = tide::with_state(app);

    app.at("/api/isalive") // 3.
        .get(api::is_alive);

    app.at("/swagger") // 3.
        .get(Redirect::permanent("/swagger/index.html"));

    app.at("/swagger/*path") // 3.
        .get(serve_static_files);

    app.at("/Tables/List").get(tables::list_of_tables);
    app.at("/Tables/Create").post(tables::create_table);
    app.at("/Tables/CreateIfNotExists")
        .post(tables::create_table_if_not_exists);
    app.at("/tables/remove").delete(tables::remove_table);

    //Bulk
    app.at("/bulk/InsertOrReplace")
        .post(bulk::insert_or_replace);

    //Row
    app.at("/Row").get(row::get_rows);
    app.at("/Row//Row/Insert").post(row::insert);
    app.at("/Row/InsertOrReplace").post(row::insert_or_replace);

    match app.listen("127.0.0.1:8080").await {
        Ok(_) => {
            println!("Application is stopped");
        }
        Err(err) => {
            println!("Can not start http server. Err is: {:?}", err);
        }
    }
}

async fn serve_static_files(req: tide::Request<Arc<AppServices>>) -> tide::Result {
    let url = req.url().as_str();
    let path = get_path(url);
    let swagger_file = format!(
        "{}/{}{}",
        std::env::current_dir().unwrap().to_str().unwrap(),
        "src",
        path
    );

    let file_result = fs::read(swagger_file).await;

    if let Ok(content) = file_result {
        let response = tide::Response::builder(200).body(content);

        let response = apply_content_type(response, url);

        return Ok(response.build());
    }

    let response = tide::Response::builder(404).body(format!("File not found: {}", path));

    return Ok(response.build());
}

fn apply_content_type(mut response: tide::ResponseBuilder, url: &str) -> tide::ResponseBuilder {
    if url.ends_with(".css") {
        response = response.content_type(tide::http::mime::CSS);
    } else if url.ends_with(".svg") {
        response = response.content_type(tide::http::mime::SVG);
    } else if url.ends_with(".png") {
        response = response.content_type(tide::http::mime::PNG);
    } else if url.ends_with(".js") {
        response = response.content_type(tide::http::mime::JAVASCRIPT);
    } else {
        response = response.content_type(tide::http::mime::HTML);
    }

    return response;
}

fn get_path(url: &str) -> &str {
    let index = url.find('/').unwrap();
    let substring = &url[index + 2..];

    let index = substring.find('/').unwrap();

    return &substring[index..];
}

/*

let swagger = fs::read(swagger_file).await.unwrap();
let response = tide::Response::builder(200)
    .body(swagger)
    .content_type(tide::http::mime::HTML)
    .build();
return Ok(response);
 */
