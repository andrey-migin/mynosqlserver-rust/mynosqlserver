use crate::db::types::PersistencePeriod;
use crate::db::{FailOperationResult, OperationResult};

fn get_ok_response(operation_result: OperationResult) -> tide::ResponseBuilder {
    return match operation_result {
        OperationResult::Ok => tide::Response::builder(200).body("OK"),
        OperationResult::OkWithJson { json } => tide::Response::builder(200)
            .body(json)
            .content_type(tide::http::mime::JSON),
    };
}

fn get_fail_response(fail_result: FailOperationResult) -> tide::ResponseBuilder {
    return match fail_result {
        FailOperationResult::TableAlreadyExist { table_name } => {
            tide::Response::builder(400).body(format!("Table '{}' already exists", table_name))
        }

        FailOperationResult::FieldPartitionKeyIsRequired => tide::Response::builder(400).body(
            format!("Field partitionKey is required to execute operation"),
        ),
        _ => tide::Response::builder(500).body(format!("{:?}", fail_result)),
    };
}

pub fn get_http_response(src: Result<OperationResult, FailOperationResult>) -> tide::Result {
    let response = match src {
        Ok(ok_result) => get_ok_response(ok_result),
        Err(fail_result) => get_fail_response(fail_result),
    };
    return Ok(response.build());
}

pub fn parse_sync_period(sync_period: Option<String>) -> PersistencePeriod {
    const DEFAULT: PersistencePeriod = PersistencePeriod::Sec5;

    if sync_period.is_none() {
        return DEFAULT;
    }

    let sync_period = sync_period.unwrap();

    return match sync_period.as_str() {
        "i" => PersistencePeriod::Immediately,
        "1" => PersistencePeriod::Sec1,
        "5" => PersistencePeriod::Sec5,
        "15" => PersistencePeriod::Sec15,
        "30" => PersistencePeriod::Sec30,
        "60" => PersistencePeriod::Min1,
        "a" => PersistencePeriod::Asap,
        _ => DEFAULT,
    };
}
