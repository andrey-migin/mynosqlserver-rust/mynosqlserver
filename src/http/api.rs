use std::{sync::Arc, time::SystemTime};

use crate::app::AppServices;

pub async fn is_alive(_: tide::Request<Arc<AppServices>>) -> tide::Result {
    let version = env!("CARGO_PKG_VERSION");

    let env_info = env!("ENV_INFO");

    let time = SystemTime::now();

    let result = format!("{}\"name\": \"MyNoSqlServer.Api\",\"version\": \"{}\",\"startedAt\": \"{:?}\",\"environment\": \"{}\"{}",
     crate::json::consts::OPEN_BRACKET as char, version, time, env_info, crate::json::consts::CLOSE_BRACKET as char);

    let result = tide::Response::builder(400)
        .body(result)
        .content_type(tide::http::mime::JSON);

    return Ok(result.build());
}
