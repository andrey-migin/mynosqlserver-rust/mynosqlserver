use std::sync::Arc;

use crate::app::AppServices;

use tide::prelude::*;

use super::http_helpers;

#[derive(Serialize, Deserialize)]
struct GetListOfRowsContract {
    #[serde(rename(deserialize = "tableName"))]
    pub table_name: String,
    #[serde(rename(deserialize = "partitionKey"))]
    pub partition_key: Option<String>,
    #[serde(rename(deserialize = "rowKey"))]
    pub row_key: Option<String>,
}

pub async fn get_rows(req: tide::Request<Arc<AppServices>>) -> tide::Result {
    let state = req.state();

    let request_model: GetListOfRowsContract = req.query()?;

    let result = state
        .db
        .get_rows(
            &request_model.table_name,
            request_model.partition_key,
            request_model.row_key,
        )
        .await;

    return http_helpers::get_http_response(result);
}

#[derive(Serialize, Deserialize)]
struct InsertContract {
    #[serde(rename(deserialize = "tableName"))]
    pub table_name: String,
    #[serde(rename(deserialize = "syncPeriod"))]
    pub sync_period: Option<String>,
}

pub async fn insert(mut req: tide::Request<Arc<AppServices>>) -> tide::Result {
    let body = req.body_bytes().await.unwrap();
    let state = req.state();

    let request_model: InsertContract = req.query()?;

    let sync_period = http_helpers::parse_sync_period(request_model.sync_period);

    let result = state.db.insert(&request_model.table_name, &body).await;

    return http_helpers::get_http_response(result);
}

pub async fn insert_or_replace(mut req: tide::Request<Arc<AppServices>>) -> tide::Result {
    let body = req.body_bytes().await.unwrap();
    let state = req.state();

    let request_model: InsertContract = req.query()?;

    let sync_period = http_helpers::parse_sync_period(request_model.sync_period);

    let result = state
        .db
        .insert_or_replace(&request_model.table_name, &body)
        .await;

    return http_helpers::get_http_response(result);
}
