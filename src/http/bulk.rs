use std::sync::Arc;

use tide::prelude::*;

use crate::{app::AppServices, db::OperationResult};

use super::http_helpers;

#[derive(Deserialize, Serialize)]
struct InsterOrReplaceModel {
    pub table_name: String,
}

pub async fn insert_or_replace(mut req: tide::Request<Arc<AppServices>>) -> tide::Result {
    let body = req.body_bytes().await.unwrap();
    let state = req.state();
    let request_model: InsterOrReplaceModel = req.query()?;
    let objects = crate::json::array_parser::split_to_objects(&body);

    todo!("Implement");
    /*
    let result = state
        .db
        .insert_or_replace(&request_model.table_name, objects)
        .await;
    return helpers::get_http_response(result);
    */
}
