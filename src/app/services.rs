use std::path::{Path, PathBuf};

use crate::db::DbInstance;

pub struct AppServices {
    pub db: DbInstance,
}

impl AppServices {
    pub fn new() -> Self {
        AppServices {
            db: DbInstance::new(),
        }
    }
}
